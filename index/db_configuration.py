import dj_database_url
import psycopg2


def get_connection():
    env = dj_database_url.config('DATABASE_URL')
    return psycopg2.connect(dbname=env['NAME'],
                            user=env['USER'],
                            password=env['PASSWORD'],
                            host=env['HOST'],
                            port=env['PORT'],
                            options=env['OPTIONS']['options'])


def close_connection(connection, cursor):
    cursor.close()
    connection.close()


class DbSingleton:
    __instance = None

    @staticmethod
    def db():
        if DbSingleton.__instance is None:
            DbSingleton()
        return DbSingleton.__instance

    def __init__(self):
        if DbSingleton.__instance is not None:
            raise Exception("Only one Database instance can be running!")
        else:
            DbSingleton.__instance = self
            env = dj_database_url.config('DATABASE_URL')
            if env.get('OPTIONS', None) is None:
                self.__access = psycopg2.connect(dbname=env['NAME'],
                                                 user=env['USER'],
                                                 password=env['PASSWORD'],
                                                 host=env['HOST'],
                                                 port=env['PORT'])
            else:
                self.__access = psycopg2.connect(dbname=env['NAME'],
                                                 user=env['USER'],
                                                 password=env['PASSWORD'],
                                                 host=env['HOST'],
                                                 port=env['PORT'],
                                                 options=env['OPTIONS']['options'])

            print('DB - OK  :: Connected to {}!'.format(env['HOST']))

    # execute SQL query
    def query(self, sql, *arg):
        print('DB - SQL :: ', sql.format(arg))  # DEBUG SQL STATEMENT
        cur = self.__access.cursor()
        try:
            cur.execute(sql, arg)
            result = None
            if sql.split(" ")[0] == "select":
                result = cur.fetchall()
        except psycopg2.Error as e:
            print(e.pgerror)
            result = None
        self.__access.commit()
        return result

    # execute SQL query
    def insert_update(self, sql):
        print('DB - SQL :: ', sql)  # DEBUG SQL STATEMENT
        cur = self.__access.cursor()
        try:
            cur.execute(sql)
        except psycopg2.Error as e:
            print(e.pgerror)
        self.__access.commit()

    # simplified execution for Stored Procedures
    def exec(self, proc, *arg):
        print('DB - SP  :: ', proc + str(arg))  # DEBUG PROC STATEMENT
        cur = self.__access.cursor()
        try:
            cur.callproc(proc, arg)
            result = cur.fetchall()
        except psycopg2.Error as e:
            print(e.pgerror)
            result = None
        self.__access.commit()
        return result
