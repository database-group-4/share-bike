"""sharebike URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.views.generic import RedirectView

from index.views import *

urlpatterns = [
    path('', index, name='index'),
    path('favicon.ico', RedirectView.as_view(url='/static/img/favicon.ico'), name='favicon'),

    path('testdb', test_db_connection, name='testdb'),

    path('shift/view', shift_vd, name='shift'),
    path('shift/addupdate', shift_cu, name="shiftcu"),
    path('shift/delete', shift_delete, name="shiftdelete"),

    path('event/view', event_vd, name='eventvd'),
    path('event/addupdate', event_cu, name="eventcu"),
    path('event/delete', event_delete, name="eventdelete"),

    path('signin', user_signin, name='login'),
    path('signout', user_logout, name='signout'),
    path('register', user_register, name='register'),

    path('sharebikepay', RedirectView.as_view(url="/sharebikepay/view"), name="sharebikeredirect"),
    path('sharebikepay/view', sharebike_pay_v, name='sharebikepayv'),
    path('sharebikepay/topup', sharebike_pay_c, name='sharebikepayc'),

    path('report', report_v, name="reportv"),

    path('station/view', station_vd, name='stationvd'),
    path('station/addupdate', station_cu, name='stationcu'),
    path('station/delete', station_delete, name='stationdelete'),

    path('bike/view', bike_vd, name='bikevd'),
    path('bike/addupdate', bike_cu, name='bikecu'),
    path('bike/delete', bike_delete, name='bikedelete'),

    path('rental/view', rental_vd, name='rentalvd'),
    path('rental/addupdate', rental_cu, name='rentalcu'),

    path('voucher/view', voucher_vd, name='vouchervd'),
    path('voucher/addupdate', voucher_cu, name='vouchercu')
]
