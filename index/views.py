import math
import random
import string
import time

import psycopg2
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect

from index.db_configuration import get_connection, close_connection, DbSingleton


# Create your views here.
def index(request):
    response = {"navlink": "home"}
    if "signed_in" in request.session:
        if request.session["signed_in"]:
            response["signed_in"] = True
            response["user_role"] = request.session["user_role"]
    else:
        response["signed_in"] = False
        request.session["signed_in"] = False
        response["user_role"] = None
        request.session["user_role"] = None
    return render(request, 'index.html', response)


def shift_vd(request):
    if request.session["signed_in"] and (
            request.session["user_role"] == "admin" or request.session["user_role"] == "staff"):
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        shift_count = DbSingleton.db().query("select count(*) from share_bike.shift;")[0][0]
        page_count = int(math.ceil(shift_count / 5))
        response = {"navlink": "shift",
                    "signed_in": True,
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            shifts = DbSingleton.db().query("select * "
                                            "from share_bike.shift "
                                            "order by {} "
                                            "limit 5 "
                                            "offset {} ;".format(sort_by, page * 5))

            response["shifts"] = shifts
            response["sort"] = request.GET["sort_by"]
        else:
            shifts = DbSingleton.db().query("select * "
                                            "from share_bike.shift "
                                            "limit 5 "
                                            "offset %s ;", page * 5)
            response["shifts"] = shifts
        staff_names = []
        station_names = []
        for shift in shifts:
            staff_names.append(DbSingleton.db().query("select p.name "
                                                      "from share_bike.staff s,"
                                                      "     share_bike.person p,"
                                                      "     share_bike.shift sf "
                                                      "where s.id = p.id"
                                                      "  and s.id = sf.id"
                                                      "  and sf.id = %s;", shift[0])[0][0])
            station_names.append(DbSingleton.db().query("select s.name "
                                                        "from share_bike.station s,"
                                                        "     share_bike.shift sf "
                                                        "where s.station_id = sf.station_id"
                                                        "  and sf.id = %s;", shift[0])[0][0])
        response["staff_names"] = staff_names
        response["station_names"] = station_names
        return render(request, 'shift-vd.html', response)
    else:
        return redirect("index")


def shift_cu(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        if request.method == "GET":
            response = {"navlink": "shift",
                        "signed_in": request.session["signed_in"],
                        "user_role": request.session["user_role"],
                        "staffs": DbSingleton.db().query("select s.id, p.name "
                                                         "from share_bike.staff s,"
                                                         "     share_bike.person p "
                                                         "where p.id = s.id"
                                                         "  and s.is_admin = false"),
                        "stations": DbSingleton.db().query("select s.station_id, s.name from share_bike.station s;"),
                        "cu_method": "create"}
            if request.GET.get("id", None) is not None:
                shift = DbSingleton.db().query("select * "
                                               "from share_bike.shift s "
                                               "where s.id = %s"
                                               "  and s.station_id = %s;",
                                               request.GET["id"],
                                               request.GET["station"])[0]
                response["shift_id"] = shift[0]
                response["shift_station"] = shift[2]
                response["shift_start"] = shift[1].strftime("%Y-%m-%dT%H:%M")
                response["shift_end"] = shift[3].strftime("%Y-%m-%dT%H:%M")
            return render(request, "shift-cu.html", response)
        else:
            shift_id = request.POST["staff"]
            shift_start = request.POST["start_date"]
            shift_end = request.POST["end_date"]
            shift_station = request.POST["station"]
            if request.POST["cu_method"] == "create":
                DbSingleton.db().query("insert into share_bike.shift (id, start_datetime, station_id, end_datetime) "
                                       "values (%s, %s, %s, %s)", shift_id, shift_start, shift_station, shift_end)
            else:
                DbSingleton.db().query("update share_bike.shift "
                                       "set start_datetime = %s ,"
                                       "    end_datetime = %s ,"
                                       "    station_id = %s "
                                       "where id = %s ;", shift_start, shift_end, shift_station, shift_id)
            return HttpResponseRedirect("/shift/view")
    else:
        return redirect("index")


def shift_delete(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        DbSingleton.db().query("delete from share_bike.shift "
                               "where id = %s "
                               "  and station_id = %s;",
                               request.GET["id"], request.GET["station"])
        return HttpResponseRedirect("/shift/view")
    else:
        return redirect("index")


def event_vd(request):
    if request.session["signed_in"]:

        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        events_count = DbSingleton.db().query("select count(*) from share_bike.event;")[0][0]
        page_count = int(math.ceil(events_count / 5))
        response = {"navlink": "event",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            response["events"] = DbSingleton.db().query("select * "
                                                        "from share_bike.event "
                                                        "order by {} "
                                                        "limit 5 "
                                                        "offset {} ;".format(sort_by, page * 5))
            response["sort"] = request.GET["sort_by"]
        else:
            response["events"] = DbSingleton.db().query("select * "
                                                        "from share_bike.event "
                                                        "limit 5 "
                                                        "offset %s ;", page * 5)

        return render(request, "event-vd.html", response)
    else:
        return redirect("index")


def event_cu(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        if request.method == "GET":
            response = {"navlink": "event",
                        "signed_in": request.session["signed_in"],
                        "user_role": request.session["user_role"],
                        "stations": DbSingleton.db().query("select s.station_id, s.name from share_bike.station s;"),
                        "cu_method": "create"}
            if request.GET.get("id", None) is not None:
                event = DbSingleton.db().query("select * "
                                               "from share_bike.event e "
                                               "where e.event_id = %s;",
                                               request.GET["id"])[0]
                event_station = DbSingleton.db().query("select s.station_id "
                                                       "from share_bike.station_event se, share_bike.station s "
                                                       "where se.event_id = %s and se.station_id = s.station_id;",
                                                       request.GET["id"])
                response["event"] = event
                response["event_station"] = event_station
                response["start_date"] = str(event[3])
                response["end_date"] = str(event[4])
                response["cu_method"] = "update"
            return render(request, "event-cu.html", response)
        else:
            event_title = request.POST["title"]
            event_free = request.POST["is_free"]
            event_description = request.POST["description"]
            event_start_date = request.POST["start_date"]
            event_end_date = request.POST["end_date"]
            if request.POST["cu_method"] == "create":
                event_id = ''.join(random.choices(string.digits, k=10))
                DbSingleton.db().query(
                    "insert into share_bike.event (event_id, title, description, start_date, end_date, is_free) "
                    "values ( %s, %s, %s, %s, %s, %s);",
                    str(event_id), event_title, event_description, event_start_date,
                    event_end_date, event_free)
                stations = request.POST.getlist("stations")
                for station_id in stations:
                    DbSingleton.db().query(
                        "insert into share_bike.station_event (station_id, event_id) VALUES (%s, %s)", station_id,
                        event_id)
            else:
                event_id = request.POST["event_id"]
                DbSingleton.db().query("update share_bike.event "
                                       "set title = %s ,"
                                       "    description = %s ,"
                                       "    start_date = %s ,"
                                       "    end_date = %s ,"
                                       "    is_free = %s "
                                       "where event_id = %s ;", event_title, event_description, event_start_date,
                                       event_end_date, event_free, event_id)
                stations = request.POST.getlist("stations")
                DbSingleton.db().query("delete from share_bike.station_event where event_id = %s", event_id)
                for station_id in stations:
                    DbSingleton.db().query(
                        "insert into share_bike.station_event (station_id, event_id) VALUES (%s, %s)", station_id,
                        event_id)
            return HttpResponseRedirect("/event/view")
    else:
        return redirect("index")


def event_delete(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        DbSingleton.db().query("delete from share_bike.event where event_id = %s", request.GET["id"])
        return HttpResponseRedirect("/event/view")
    else:
        return redirect("index")


def rental_vd(request):
    if request.session["signed_in"]:
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        rental_count = DbSingleton.db().query("select count(*) from share_bike.rental;")[0][0]
        page_count = int(math.ceil(rental_count / 5))
        response = {"navlink": "event",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            rentals = DbSingleton.db().query("select * "
                                             "from share_bike.rental "
                                             "order by {} "
                                             "limit 5 "
                                             "offset {} ;".format(sort_by, page * 5))

            response["sort"] = request.GET["sort_by"]
        else:
            rentals = DbSingleton.db().query("select * "
                                             "from share_bike.rental "
                                             "limit 5 "
                                             "offset %s ;", page * 5)
        bikes = []
        stations = []
        for rental in rentals:
            bikes.append(DbSingleton.db().query("select b.number, b.brand "
                                                "from share_bike.bike b, share_bike.rental r "
                                                "where b.number = r.bike_num"
                                                "  and r.bike_num = %s"
                                                "  and r.member_card_num = %s "
                                                "  and r.rent_datetime = %s"
                                                "  and r.station_id = %s;",
                                                rental[4], rental[0], rental[1], rental[2])[0])
            stations.append(DbSingleton.db().query("select s.station_id, s.name "
                                                   "from share_bike.station s, share_bike.rental r "
                                                   "where s.station_id = r.station_id"
                                                   "  and r.bike_num = %s"
                                                   "  and r.member_card_num = %s "
                                                   "  and r.rent_datetime = %s"
                                                   "  and r.station_id = %s;",
                                                   rental[4], rental[0], rental[1], rental[2])[0])
        response["bikes"] = bikes
        response["stations"] = stations
        response["rentals"] = rentals
        return render(request, "rental-vd.html", response)
    else:
        return redirect("index")


def rental_cu(request):
    if request.session["signed_in"] and request.session["user_role"] == "member":
        if request.method == "GET":
            response = {"navlink": "rental",
                        "signed_in": request.session["signed_in"],
                        "user_role": request.session["user_role"],
                        "stations": DbSingleton.db().query("select s.station_id, s.name from share_bike.station s;"),
                        "cu_method": "create"}
            if request.GET.get("id", None) is not None:
                pass
            return render(request, "rental-cu.html", response)
        else:
            bike_id = request.POST["id"]
            member_card = request.session["member_card"]
            station_id = DbSingleton.db().query("select b.station_id "
                                                "from share_bike.bike b "
                                                "where b.number = %s;", bike_id)[0][0]
            if request.POST["cu_method"] == "create":
                DbSingleton.db().query(
                    "insert into rental "
                    "(member_card_num, rent_datetime, station_id, bike_num) "
                    "values "
                    "(%s, %s, %s, %s)", member_card, time.time(), station_id, bike_id)
            else:
                pass
            return redirect("rentalvd")
    else:
        return redirect("index")


def voucher_vd(request):
    if request.session["signed_in"]:
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        voucher_count = DbSingleton.db().query("select count(*) from share_bike.voucher;")[0][0]
        page_count = int(math.ceil(voucher_count / 5))
        response = {"navlink": "voucher",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "member_card": request.session["member_card"],
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            response["vouchers"] = DbSingleton.db().query("select * "
                                                          "from share_bike.voucher "
                                                          "order by {} "
                                                          "limit 5 "
                                                          "offset {} ;".format(sort_by, page * 5))
            response["sort"] = request.GET["sort_by"]
        else:
            response["vouchers"] = DbSingleton.db().query("select * "
                                                          "from share_bike.voucher "
                                                          "limit 5 "
                                                          "offset %s ;", page * 5)
        return render(request, "voucher-vd.html", response)
    else:
        return redirect("index")


def voucher_cu(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        if request.method == "GET":
            response = {"navlink": "voucher",
                        "signed_in": request.session["signed_in"],
                        "user_role": request.session["user_role"],
                        "vouchers": DbSingleton.db().query("select v.voucher_id, from share_bike.voucher v;"),
                        "cu_method": "create"}
            if request.GET.get("id", None) is not None:
                voucher = DbSingleton.db().query("select * "
                                                 "from share_bike.voucher v"
                                                 "where v.voucher_id = %s;",
                                                 request.GET["id"])[0]
                response["name"] = str(voucher[1])
                response["category"] = str(voucher[2])
                response["points value"] = str(voucher[3])
                response["amount"] = str(voucher[4])
                response["description"] = str(voucher[5])
                response["cu_method"] = "update"
            return render(request, "voucher-cu.html", response)
        else:
            voucher_name = request.POST["name"]
            voucher_category = request.POST["category"]
            voucher_pointsvalue = request.POST["points value"]
            voucher_amount = request.POST["amount"]
            voucher_description = request.POST["description"]
            if request.POST["cu_method"] == "create":
                voucher_id = ''.join(random.choices(string.digits, k=10))
                DbSingleton.db().query(
                    "insert into share_bike.voucher (voucher_id, name, category, points value, amount, description) "
                    "values ( %s, %s, %s, %s, %s, %s);",
                    str(voucher_id), voucher_name, voucher_category, voucher_pointsvalue, voucher_amount,
                    voucher_description)
            else:
                voucher_id = request.POST["voucher_id"]
                DbSingleton.db().query("update share_bike.event "
                                       "set name = %s ,"
                                       "    category = %s ,"
                                       "    points value = %s ,"
                                       "    amount = %s ,"
                                       "    description = %s "
                                       "where voucher_id = %s ;", voucher_name, voucher_category, voucher_pointsvalue,
                                       voucher_amount, voucher_description)
                DbSingleton.db().query("delete from share_bike.station_event where voucher_id = %s", voucher_id)
            return HttpResponseRedirect("/voucher/view")
    else:
        return redirect("index")


def voucher_delete(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        DbSingleton.db().query("delete from share_bike.voucher where voucher_id = %s", request.GET["id"])
        return HttpResponseRedirect("/event/view")
    else:
        return redirect("index")


def test_db_connection(request):
    try:
        connection = get_connection()
        cursor = connection.cursor()
    except psycopg2.Error:
        return JsonResponse({'success': False})
    close_connection(connection, cursor)
    return JsonResponse({'success': True})


def user_signin(request):
    if request.method == "POST":
        user_id = request.POST.get("userId")
        email = request.POST.get("email") + "@" + request.POST.get("domain")
        person = DbSingleton.db().query("select * from share_bike.person p "
                                        "where p.id = %s and p.email = %s", user_id, email)
        if len(person) > 0:
            member = DbSingleton.db().query("select * from share_bike.member where id = %s ;", user_id)
            if len(member) > 0:
                print("member found")
                request.session["signed_in"] = True
                request.session["user_role"] = "member"
                request.session["member_card"] = member[0][0]
                return redirect("bikevd")
            else:
                staff = DbSingleton.db().query("select * from share_bike.staff where id = %s ;", user_id)
                if staff[0][2]:
                    print("admin found")
                    request.session["signed_in"] = True
                    request.session["user_role"] = "admin"
                    return redirect("rentalvd")
                else:
                    print("staff found")
                    request.session["signed_in"] = True
                    request.session["user_role"] = "staff"
                    return redirect("shift")
        else:
            print("person not found")
            return redirect("index")
    else:
        return redirect("index")


def user_logout(request):
    request.session["signed_in"] = False
    request.session["user_role"] = None
    return redirect("index")


def user_register(request):
    if request.method == "POST":
        user_role = request.POST["role"]
        user_id = request.POST["id"]
        user_name = request.POST["name"]
        user_email = request.POST.get("email") + "@" + request.POST.get("domain")
        user_birthdate = request.POST["birthdate"]
        user_phone = request.POST["phone"] if request.POST["phone"] else None
        user_address = request.POST["address"] if request.POST["address"] else None

        if not user_id or not user_name or not user_email or not user_birthdate:
            print("Some required collumns are not filled")
            return redirect("index")

        # validate duplicate
        person = DbSingleton.db().query("select * from share_bike.person where id = %s ;", user_id)
        if len(person) != 0:
            print("id already exist")
            return redirect("index")
        person = DbSingleton.db().query("select * from share_bike.person where email = %s ;", user_email)
        if len(person) != 0:
            print("email already exist")
            return redirect("index")

        # insert
        DbSingleton.db().query("insert into share_bike.person (id, email, name, address, birth_date, phone_number) "
                               "values (%s, %s, %s, %s, %s, %s);",
                               user_id, user_email, user_name, user_address, user_birthdate, user_phone)
        if user_role == "member":
            member_card = ''.join(random.choices(string.digits, k=10))
            DbSingleton.db().query("insert into share_bike.member (card_number, balance, points, id) "
                                   "values (%s, %s, %s, %s)", member_card, 0, 0, user_id)
            print("Member successfully registered")
            request.session["signed_in"] = True
            request.session["user_role"] = "member"
            request.session["member_card"] = member_card
            return redirect("bikevd")
        elif user_role == "staff":
            DbSingleton.db().query("insert into share_bike.staff (id, salary, is_admin) values (%s, %s, %s)",
                                   user_id, 30000, False)
            print("Staff successfully registered")
            request.session["signed_in"] = True
            request.session["user_role"] = "staff"
            return redirect("shift")
        elif user_role == "admin":
            DbSingleton.db().query("insert into share_bike.staff (id, salary, is_admin) values (%s, %s, %s)",
                                   user_id, 0, True)
            print("Admin successfully registered")
            request.session["signed_in"] = True
            request.session["user_role"] = "admin"
            return redirect("rentalvd")
        else:
            print("Role not found")
            return redirect("index")
    else:
        return redirect("index")


def sharebike_pay_v(request):
    if request.session["signed_in"] and request.session["user_role"] == "member":
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        events_count = DbSingleton.db().query("select count(*) from share_bike.transaction where member_card_num = %s;",
                                              request.session["member_card"])[0][0]
        page_count = int(math.ceil(events_count / 5))
        response = {"navlink": "sharebikepay",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            response["transactions"] = DbSingleton.db().query("select * "
                                                              "from share_bike.transaction "
                                                              "where member_card_num = %s "
                                                              "order by {} "
                                                              "limit 5 "
                                                              "offset {} ;".format(sort_by, page * 5),
                                                              request.session["member_card"])
            response["sort"] = request.GET["sort_by"]
        else:
            response["transactions"] = DbSingleton.db().query("select * "
                                                              "from share_bike.transaction "
                                                              "where member_card_num = %s "
                                                              "limit 5 "
                                                              "offset %s ;", request.session["member_card"], page * 5)
        return render(request, "sharebikepay-v.html", response)
    else:
        return redirect("index")


def sharebike_pay_c(request):
    if request.session["signed_in"] and request.session["user_role"] == "member":
        response = {"navlink": "sharebikepay",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"]}

        if request.method == "GET":
            return render(request, "sharebikepay-c.html", response)
        else:
            amount = request.POST["amount"]
            DbSingleton.db().query("insert into share_bike.transaction (member_card_num, date_time, type, nominal) "
                                   "values (%s, to_timestamp(%s), %s, %s);",
                                   request.session["member_card"], time.time(), "TopUp", amount)
            return redirect("sharebikepayv")
    else:
        return redirect("index")


def report_v(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        events_count = DbSingleton.db().query("select count(*) from share_bike.report;")[0][0]
        page_count = int(math.ceil(events_count / 5))
        response = {"navlink": "report",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            reports = DbSingleton.db().query("select * "
                                             "from share_bike.report "
                                             "order by {} "
                                             "limit 5 "
                                             "offset {} ;".format(sort_by, page * 5))
            response["sort"] = request.GET["sort_by"]
        else:
            reports = DbSingleton.db().query("select * "
                                             "from share_bike.report "
                                             "limit 5 "
                                             "offset %s ;", page * 5)
        names = []
        fines = []
        for report in reports:
            names.append(DbSingleton.db().query("select p.name "
                                                "from share_bike.person p,"
                                                " share_bike.member m,"
                                                " share_bike.report r "
                                                "where p.id = m.id"
                                                "  and m.card_number = r.member_card_num"
                                                "  and r.member_card_num = %s;", report[1])[0][0])
            fines.append(DbSingleton.db().query("select rt.fines "
                                                "from share_bike.rental rt,"
                                                "     share_bike.report rp "
                                                "where rt.bike_num = rp.bike_num"
                                                "  and rt.rent_datetime = rp.rent_datetime"
                                                "  and rt.station_id = rp.station_id"
                                                "  and rt.member_card_num = rp.member_card_num"
                                                "  and rp.report_id = %s;", report[0])[0][0])
        response["fines"] = fines
        response["reports"] = reports
        response["names"] = names
        return render(request, "report-v.html", response)
    else:
        return redirect("index")


def station_vd(request):
    if request.session["signed_in"]:
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        station_count = DbSingleton.db().query("select count(*) from share_bike.station;")[0][0]
        page_count = int(math.ceil(station_count / 5))
        response = {"navlink": "station",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            response["stations"] = DbSingleton.db().query("select * "
                                                          "from share_bike.station "
                                                          "order by {} "
                                                          "limit 5 "
                                                          "offset {} ;".format(sort_by, page * 5))
            response["sort"] = request.GET["sort_by"]
        else:
            response["stations"] = DbSingleton.db().query("select * "
                                                          "from share_bike.station "
                                                          "limit 5 "
                                                          "offset %s ;", page * 5)
        return render(request, "station-vd.html", response)
    else:
        return redirect("index")


def station_cu(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        if request.method == "GET":
            response = {"navlink": "event",
                        "signed_in": request.session["signed_in"],
                        "user_role": request.session["user_role"],
                        "cu_method": "create"}
            if request.GET.get("id", None) is not None:
                station = DbSingleton.db().query("select * "
                                                 "from share_bike.station s "
                                                 "where s.station_id = %s;",
                                                 request.GET["id"])[0]
                response["id"] = request.GET["id"]
                response["name"] = station[4]
                response["address"] = station[1]
                response["lat"] = station[2]
                response["long"] = station[3]
                response["cu_method"] = "update"
            return render(request, "station-cu.html", response)
        else:
            station_id = request.POST["id"]
            station_address = request.POST["address"]
            station_lat = request.POST["lat"]
            station_long = request.POST["long"]
            station_name = request.POST["name"]
            if request.POST["cu_method"] == "create":
                DbSingleton.db().query(
                    "insert into share_bike.station (station_id, address, lat, long, name) "
                    "values ( %s, %s, %s, %s, %s);", ''.join(random.choices(string.digits, k=10)),
                    station_address, station_lat, station_long, station_name)
            else:
                DbSingleton.db().query("update share_bike.station "
                                       "set address = %s ,"
                                       "    lat = %s ,"
                                       "    long = %s ,"
                                       "    name = %s "
                                       "where station.station_id = %s ;",
                                       station_address, station_lat, station_long, station_name, station_id)
            return HttpResponseRedirect("/station/view")
    else:
        return redirect("index")


def station_delete(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        DbSingleton.db().query("delete from share_bike.station where station_id = %s", request.GET["id"])
        return HttpResponseRedirect("/station/view")


def bike_vd(request):
    if request.session["signed_in"]:
        page = int(request.GET.get("page", 0)) - 1
        page = 0 if page < 0 else page
        bike_count = DbSingleton.db().query("select count(*) from share_bike.bike;")[0][0]
        page_count = int(math.ceil(bike_count / 5))
        response = {"navlink": "bike",
                    "signed_in": request.session["signed_in"],
                    "user_role": request.session["user_role"],
                    "pages": range(1, page_count + 1),
                    "current_page": page + 1}
        if request.GET.get("sort_by", None) is not None and request.GET.get("sort_by", None) != "Sort by":
            sort_by = request.GET["sort_by"]
            bikes = DbSingleton.db().query("select * "
                                           "from share_bike.bike "
                                           "order by {} "
                                           "limit 5 "
                                           "offset {} ;".format(sort_by, page * 5))

            response["sort"] = request.GET["sort_by"]
        else:
            bikes = DbSingleton.db().query("select * "
                                           "from share_bike.bike "
                                           "limit 5 "
                                           "offset %s ;", page * 5)
        donators = []
        stations = []
        for bike in bikes:
            donators.append(DbSingleton.db().query("select p.name "
                                                   "from share_bike.person p,"
                                                   "     share_bike.member m,"
                                                   "     share_bike.bike b "
                                                   "where p.id = m.id"
                                                   "  and m.card_number = b.donator_card_num"
                                                   "  and b.number = %s", bike[0])[0][0])
            stations.append(DbSingleton.db().query("select s.name "
                                                   "from share_bike.station s,"
                                                   "     share_bike.bike b "
                                                   "where s.station_id = b.station_id"
                                                   "  and b.number = %s;", bike[0])[0][0])
        response["stations"] = stations
        response["donators"] = donators
        response["bikes"] = bikes
        return render(request, "bike-vd.html", response)
    else:
        return redirect("index")


def bike_cu(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        if request.method == "GET":
            response = {"navlink": "bike",
                        "signed_in": request.session["signed_in"],
                        "user_role": request.session["user_role"],
                        "stations": DbSingleton.db().query("select s.station_id, s.name from share_bike.station s;"),
                        "donators": DbSingleton.db().query("select m.card_number, p.name "
                                                           "from share_bike.member m, share_bike.person p "
                                                           "where m.id = p.id;"),
                        "cu_method": "create"}
            if request.GET.get("id", None) is not None:
                bike = DbSingleton.db().query("select * "
                                              "from share_bike.bike b "
                                              "where b.number = %s;",
                                              request.GET["id"])[0]
                response["bike_id"] = bike[0]
                response["bike_brand"] = bike[1]
                response["bike_type"] = bike[2]
                response["bike_status"] = bike[3]
                response["bike_station"] = bike[4]
                response["bike_donator"] = bike[5]
                response["cu_method"] = "update"
            return render(request, "bike-cu.html", response)
        else:
            bike_number = request.POST["id"]
            bike_brand = request.POST["brand"]
            bike_type = request.POST["type"]
            bike_status = request.POST["status"]
            bike_station = request.POST["station"]
            bike_donator = request.POST["donator"]
            if request.POST["cu_method"] == "create":
                DbSingleton.db().query(
                    "insert into share_bike.bike (number, brand, type, status, station_id, donator_card_num) "
                    "values (%s, %s, %s, %s, %s, %s)",
                    ''.join(random.choices(string.digits, k=10)), bike_brand, bike_type, bike_status, bike_station,
                    bike_donator)
            else:
                DbSingleton.db().query(
                    "update share_bike.bike "
                    "set brand = %s,"
                    "    type = %s,"
                    "    status = %s,"
                    "    station_id = %s,"
                    "    donator_card_num = %s "
                    "where number = %s;", bike_brand, bike_type, bike_status, bike_station, bike_donator, bike_number)
        return HttpResponseRedirect("/bike/view")
    else:
        return redirect("index")


def bike_delete(request):
    if request.session["signed_in"] and request.session["user_role"] == "admin":
        DbSingleton.db().query("delete from share_bike.bike where number = %s;", request.GET["id"])
        return HttpResponseRedirect("/bike/view")
    else:
        return redirect("index")
