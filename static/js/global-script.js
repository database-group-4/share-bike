$(document).ready(function () {
    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    let idValid = false;
    let emailValid = false;

    let submitSelector = $("#sign-in-submit");
    $("#sign-in-id").focusout(function () {
        let idSelector = $("#sign-in-id");
        let regex = new RegExp("[1234567890]*");
        // if (regex.test(idSelector.val()) && idSelector.val().length === 16) {
        if (regex.test(idSelector.val())) {
            idValid = true;
            idSelector.popover('hide');
            if (submitSelector[0].hasAttribute("disabled") && idValid && emailValid) {
                submitSelector.removeAttr("disabled");
            }
        } else {
            idValid = false;
            idSelector.popover('show');
            $("#sign-in-submit").attr("disabled", "")
        }
    });

    $("#full-email").focusout(function () {
        let emailSelector = $("#sign-in-email");
        let domainSelector = $("#sign-in-domain");
        let emailRegex = new RegExp("^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))");
        let domainRegex = new RegExp("((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
        if (emailRegex.test(emailSelector.val()) && emailSelector.val().length > 0 && domainRegex.test(domainSelector.val())) {
            emailValid = true;
            $("#full-email").popover("hide");
            if (submitSelector[0].hasAttribute("disabled") && idValid && emailValid) {
                submitSelector.removeAttr("disabled");
            }
        } else {
            emailValid = false;
            $("#full-email").popover("show");
            $("#sign-in-submit").attr("disabled", "")
        }
    });
});